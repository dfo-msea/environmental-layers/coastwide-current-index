# Name: SplineBarriers.py
# Description: Interpolate a series of point features onto a
#    rectangular raster using a barrier, using a
#    minimum curvature spline technique.
# Requirements: Spatial Analyst Extension and Java Runtime
# Author: Jessica Nephin

# Import system modules
import os
import arcpy
from arcpy import env
from arcpy.sa import *
import sys

# set workspace
#os.chdir("..")


#------------------------------------------------------------------#

# Set variables
infeature = os.getcwd()+"/Shapefiles/CombinedModels_CurrentSpeeds.shp"
outfeature = os.getcwd()+"/Coastwide_CurrentSpeedIndex.tif"
inBarrierFeature = os.getcwd()+"/Boundary/coast_buffer_1km.shp"
cellSize = 20.0
smoothing = 1
field_name = "Combined"

# Check out the ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

# Execute Spline with Barriers
outSB = SplineWithBarriers(infeature, field_name, inBarrierFeature, cellSize, smoothing)
# Save the output
outSB.save(outfeature)

# Mask
inMask = os.getcwd()+"/Boundary/Nearshore_Boundary_50mbuffer.shp"
outMask = os.getcwd()+"/Nearshore_CurrentSpeedIndex_Masked.tif"
# Extract
outExtractByMask = ExtractByMask(outSB, inMask)
# Save the output
outExtractByMask.save(outMask)

# Reclass negatives
outFinal = os.getcwd()+"/Nearshore_CurrentSpeedIndex.tif"
ras = Raster("Nearshore_CurrentSpeedIndex_Masked.tif")
outCon = Con(ras <= 0, 0, ras)
outCon.save(outFinal)


# Adding source to combined point layer (calculate field)
def Reclass(speed_ms, speed_ms_1, botcurr_ms, meanSummer, tidal_sum_):
    if (speed_ms > 0):
        return 'NCC FVCOM'
    elif (speed_ms_1 > 0):
        return 'QCS FVCOM'
    elif (botcurr_ms > 0):
        return 'WCVI FVCOM'
    elif (meanSummer > 0):
        return 'BC ROMS'
    elif (tidal_sum_ > 0):
        return 'SoG NEMO'
    else:
        return ''
 
 Source = Reclass(!speed_ms!, !speed_ms_1!, !botcurr_ms!, !meanSummer!, !tidal_sum_!)
                  
                  
# Adding source ID (calculate field)
def Reclass(Source):
    if (Source == 'NCC FVCOM'):
        return 1
    elif (Source == 'QCS FVCOM'):
        return 2
    elif (Source == 'WCVI FVCOM'):
        return 3
    elif (Source == 'BC ROMS'):
        return 4
    elif (Source == 'SoG NEMO'):
        return 5
    else:
        return 0
 
 Source_ID = Reclass(!Source!)