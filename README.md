# Methods for creating coastwide nearshore bottom current speed index layer

__Main author:__  Jessica Nephin  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Source Data](#sourcedata)
- [Methods](#methods)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
To produce a bottom current speed layer from the best available model data that spans the nearshore species modelling domains used by the Marine Spatial Ecology and Analysis section.

## Summary
We combined outputs from five source models to create a bottom current speed index layer that spanned the nearshore areas of the BC coastline.

## Status
Complete

## Contents
Much of the analysis was done in ArcPro manually, see 'Coastwide_CurrentSpeed.aprx'. The interpolation and clipping of the index layer was completed using the spine-barriers.py script. There are two outputs, one current speed raster layer (Nearshore_CurrentSpeedIndex.tif) and one raster layer that shows which areas were sourced from which ocean models (Nearshore_CurrentSpeedIndex_SourceData.tif). The source data raster layer can be used to understand which areas more uncertain and where the boundaries between the source data lie.

## Source Data

| Name       | Data source                       | Resolution      |  Years    | Months             | Vertical levels | Publication                                                          |
| ---------- | --------------------------------- | --------------- | --------- | ------------------ | --------------- | -------------------------------------------------------------------- |
| BC ROMS    | Isaak Fain/Diane Masson (OSD)     | ~3 km           | 1998-2007 | April to September | 30 sigma levels | https://doi.org/10.1029/2012JC008151                                 |
| NCC FVCOM  | Pramod Thupaki (OSD)              | <100m to 1km    | ?         | Summer (29 days)   | 21 sigma layers |                                                                      |
| WCVI FVCOM | Laura Bianucci/ Mike Foreman (OSD)| <60 m to 9.2 km | 2016      | March to July      | 20 sigma layers | https://waves-vagues.dfo-mpo.gc.ca/library-bibliotheque/41033656.pdf |
| QCS FVCOM  | Laura Bianucci (OSD)              | 9.6 m to 1.9 km | 2019      | July               | 20 sigma layers | https://doi.org/10.1080/07055900.2023.2184321                        |
| SoG NEMO   | UBC/MEOPAR (erdap site)           | 440 m by 500 m  | 2017      | April to September | 40 z-levels     | https://dx.doi.org/10.1080/07055900.2015.1108899                     |


## Methods
* Convert ocean model output (u and v velocities) into mean bottom current speed (m/s) using root mean square
* Identify the areas where each nearshore model should be used (the focus area of the model and close to shore)
* For overlapping areas (NCC and QCS) select the prefered model to use in that area
* Clip each nearshore model points to only include the selected areas (selected NCC over QCS in the central coast area where they overlapped)
* Clip points from the BC ROMS shapefile that overlap with any of the nearshore model areas
* Combine the clipped individual point shapefiles from each model into a single shapefile, with a common current speed attribute 
* Interpolate the combined points using spline with barriers, with a 1km buffered and smoothed coastline, to a 20m raster
* Clip the raster to the nearshore modelling domain
* Reclassify any negative values to zero
* Create a source data raster from the combined shapefile using the source data field by creating thiessen polygons, converting to raster, and masking.

# Caveats
* The Nearshore_CurrentSpeedIndex was intented for nearshore species distribution modelling 
* The layer combines the best available model output for each area, but because the source model varies by region, this layer cannot be thought of as a continuous coastwide current speed layer
* The qaulity of the index likely varies by region
* Because the source models vary in resolution, timing, and boundary and atmospheric forcing the rlative magnitude of the index my vary between regions
* This layer is meant to represent the general pattern of high and low current speed along the coast

# Uncertainty
The current speed index is more uncertain around Haida Gwaii, the north coast and north west coast where the BC ROMS model is the source data

# Acknowledgements
* Ashley Park for initiating the project and contribution towards methods
* Laura Bianucci for sharing source data and warnings about potential issues when combining outputs from different models
* Isaak Fain, Diane Masson, Pramod Thupaki, Mike Forman, UBC/MEOPAR for sharing the source data


